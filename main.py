"""
Usage: calculator.py <build_number> <path> <exp_fictivious> <exp_newacwidget> <exp_vmp> <exp_filter> <exp_wins>
    <build_number>  Jenkins build number
    <path>          Path to event archives on S3
    <exp_...>       Expected event count per apptst
    -h              help
"""
import logging
from time import time
from docopt import docopt

from calculator import Calculator
from event_handler import EventHandler

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

if __name__ == '__main__':
    start = time()
    arguments = docopt(__doc__)
    print(arguments)
    path = arguments.get("<path>")
    pattern = "nginx_load_test_" + arguments.get("<build_number>")
    mount_point = "/mnt/qa"
    work_dir = "/tmp/event_count"

    paths = {}
    paths['wins'] = mount_point + "/anyclip-dsp-qa/events/dsp/adserver/input/win_notification/" + path
    paths['filter'] = mount_point + "/anyclip-filter-server-qa/events/widget/" + path
    paths['newacwidget'] = mount_point + "/anyclip-dsp-qa/events/widget/" + path
    paths['vmp'] = mount_point + "/anyclip-vmp-qa/events/vb/widget/" + path
    paths['fictivious'] = mount_point + "/anyclip-fictivious/qa/" + path

    calc = Calculator()
    eh = EventHandler()
    act_events = eh.process_events(paths, work_dir, pattern)
    exp_events = eh.create_expected_counter(paths, arguments.get("<exp_wins>"), arguments.get("<exp_fictivious>"),
                                            arguments.get("<exp_filter>"), arguments.get("<exp_newacwidget>"),
                                            arguments.get("<exp_vmp>"))

    diffs = calc.calculate_diffs(exp_events, act_events)
    logger.info("Diffs: " + str(diffs))

    end = int(time()) - int(start)
    print "Run time " + str(end) + " sec."
