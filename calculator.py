import logging

from logger import Logger


class Calculator(Logger):

    logger = logging.getLogger(__name__)

    def __init__(self):
        self._actual = None
        self._expected = None

    def calculate_diffs(self, expected, actual):
        diffs = {}
        assert len(actual) == len(expected), "Expected and Actual sizes are not equal.\n " \
                                             "Actual: " + str(actual) + "\nExpected: " + str(expected)
        for event in actual:
            diff = actual[event] - expected[event]
            if diff != 0:
                diffs[event] = diff
        self.logger.info(diffs)
        return diffs
