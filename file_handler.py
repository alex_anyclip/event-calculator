import logging
import os
from shutil import copytree, rmtree
from logger import Logger


class FileHandler(Logger):

    logger = logging.getLogger(__name__)

    def __init__(self):
        pass

    def delete(self, path):
        self.logger.info("Deleting " + path)
        rmtree(path=path, ignore_errors=True)
        self.logger.info("Done.")

    def create(self, path):
        self.logger.info("Creatinging " + path)
        if not os.path.exists(path):
            os.makedirs(path)
            self.logger.info(path + " already exists")
        self.logger.info("Done.")

    def copy_files(self, src, dst):
        self.logger.info("Copying from " + src + " to " + dst)
        copytree(src=src, dst=dst)
        self.logger.info("Done.")

