import gzip
import logging
import os

from file_handler import FileHandler
from logger import Logger


class EventHandler(Logger):
    logger = logging.getLogger(__name__)

    def __init__(self):
        pass

    def process_events(self, paths, work_dir, pattern):
        act_results = {}
        fh = FileHandler()
        for event in paths:
            act_results[event] = 0
            fh.delete(work_dir)
            fh.copy_files(paths.get(event), work_dir)
            act_results[event] = self._count_events(event, work_dir, pattern)
        return act_results

    def _count_events(self, event_name, work_dir, pattern):
        self.logger.info("Counting " + event_name + " events")
        counter = 0
        for file_name in os.listdir(work_dir):
            with gzip.open(work_dir + "/" + file_name, 'r') as my_file:
                for row in my_file:
                    if pattern in row:
                        counter += 1
        self.logger.info("Found " + str(counter) + " " + event_name + " events")
        self.logger.info("Done.")
        return counter

    def create_expected_counter(self, paths, wins, fictivious, filter, newacwidget, vmp):
        exp_results = {}
        for path in paths:
            exp_results[path] = int(eval(path))
        return exp_results
